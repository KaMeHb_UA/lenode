module.exports = ({
    getPrivate,
    setPrivate,
    logger,
    prefix,
    importer,
    http,
    checkType,
    pkg,
    Cookies,
}) => {
    class LeNode{
        constructor(settings){
            if(!settings.router) throw new ReferenceError('settings.router is not defined');
            setPrivate(this, 'settings', settings);
            this.logger = logger || new class Logger{
                log(data){
                    console.log(prefix({
                        r: 0,
                        g: 200,
                        b: 0
                    }) + data)
                }
                warn(data){
                    console.warn(prefix({
                        r: 255,
                        g: 100,
                        b: 0
                    }) + data)
                }
                err(data){
                    console.error(prefix({
                        r: 255,
                        g: 0,
                        b: 0
                    }) + data)
                }
            }
        }
        start(port){
            var settings = getPrivate(this, 'settings');
            if (!checkType(settings.router, Array)) throw new TypeError('router is not configured propertly');
            var addSite = (exp, site) => {
                var tpmStack = getPrivate(this, 'siteStack') || [];
                tpmStack.push({
                    domain: exp,
                    site
                });
                setPrivate(this, 'siteStack', tpmStack);
            }
            http.createServer(async (req, res) => {
                var req_body = req.method == 'POST' || req.method == 'PUT' ? new Promise(resolve => {
                    let body = [];
                    req.on('data', chunk => {
                        body.push(chunk)
                    }).on('end', () => {
                        resolve(Buffer.concat(body).toString())
                    })
                }) : null;
                var [host] = (req.headers.host || '').split(':'),
                    [address, GETparams] = (req.url || '').split('?'),
                    site = null,
                    protocol = req.connection.encrypted ? 'https' : 'http';
                function setHead(name, val){
                    try{res.setHeader(name, val)}catch(e){}
                }
                getPrivate(this, 'siteStack').forEach(sitedef => {
                    if(!site && sitedef.domain.test(host)) site = sitedef.site;
                });
                if (site){
                    try{
                        let {result, code} = await site.getPage({
                            address,
                            method: req.method,
                            POST: (req.method != 'POST' ? null : (data => {
                                const types = {
                                    'application/x-www-form-urlencoded': data => {
                                        var a = {};
                                        decodeURIComponent(data.toString().replace(/\+/g, '%20')).split('&').forEach(e => {
                                            e = e.split('=');
                                            a[e.shift()] = e.join('=') || null
                                        });
                                        return a
                                    },
                                    'application/json': data => {
                                        var a = null;
                                        try {
                                            a = JSON.parse(data)
                                        } catch(e){}
                                        return a;
                                    }
                                };
                                return types[req.headers["content-type"]] ? types[req.headers["content-type"]](data) : null;
                            })(await req_body)),
                            GET: (getArr => {
                                var res = {};
                                getArr.forEach(getArg => {
                                    getArg = getArg.split('=');
                                    res[decodeURIComponent(getArg.shift())] = decodeURIComponent(getArg.join('='));
                                })
                                return res;
                            })((GETparams || '').split('&')),
                            REQUEST: {},
                            COOKIE: Cookies.from(req),
                            port,
                            protocol,
                            fullAddress: `${protocol}://${req.headers.host}${req.url}`,
                            headers: req.headers,
                            response: res,
                            request: req,
                            clientIP: req.headers['x-forwarded-for'] ||
                                req.connection.remoteAddress ||
                                req.socket.remoteAddress ||
                                (req.connection.socket ? req.connection.socket.remoteAddress : null)
                        });
                        res.statusCode = code || 200;
                        setHead('Content-Type', 'text/html; charset=utf-8');
                        res.end(result)
                    } catch (err){
                        this.logger.err(err);
                        res.statusCode = 500;
                        setHead('Content-Type', 'text/html; charset=utf-8');
                        res.end(`<html><head><meta charset="utf-8"></head><body>Internal server error.${
                            process.env.NODE_ENV == 'dev' ? `<br/>Error stack:<hr><div style="overflow:auto;"><pre>${err.stack}</pre></div>` : ''
                        }<hr><h3 style="text-align:center;">LeNode server ${pkg.version}</h3></body></html>`);
                    }
                }
                //this.logger.log({req, res})
            }).listen(port);
            settings.router.forEach(async site => {
                if (!checkType(site.domain, RegExp) || !checkType(site.folder, String)){
                    throw new TypeError('router is not configured propertly')
                } else {
                    let siteObj = new (await importer(site.folder))();
                    siteObj.setServerInstanse(this);
                    addSite(site.domain, siteObj);
                }
            });
        }
    }
    class Site{
        constructor(){}
        setServerInstanse(server){
            setPrivate(this, 'system::serverInstanse', server);
            this.logger = server.logger;
            this.init();
        }
        init(){}
    }
    return { LeNode, Site }
}
