const internalCookiesList = Symbol('InternalCookiesList'),
    IncomingMessage = require('http').IncomingMessage,
    _ = (() => class Cookies{
        constructor(cookies){
            var list = {};
            cookies && cookies.split(';').forEach(cookie => {
                var parts = cookie.split('=');
                list[decodeURIComponent(parts.shift().trim())] = decodeURIComponent(parts.join('='))
            });
            this[internalCookiesList] = list;
        }
        [Symbol.toPrimitive](){
            var res = '';
            for(var i in this[internalCookiesList]){
                res += `${encodeURIComponent(i)}=${encodeURIComponent(this[internalCookiesList][i])};`;
            }
            return res.slice(0, -1)
        }
    })();

function checkCType(val){
    return typeof val == 'string' || typeof val == 'number'
}

module.exports = class Cookies{
    /**
     * @param {String} cookies 
     */
    constructor(cookies){
        return new Proxy(new _(cookies), {
            get(target, name){
                if(typeof name == 'string') return target[internalCookiesList][name] || target[name];
                return target[name];
            },
            set(target, name, value){
                if(!checkCType(name) || !checkCType(value)) throw new TypeError('Cannot set non-string or non-num cookie');
                target[internalCookiesList][name] = value + '';
                return true
            }
        })
    }
    /**
     * @param {IncomingMessage|String} resource
     * @return {Cookies}
     */
    static from(resource){
        if(typeof resource == 'string'){
            return new Cookies(resource);
        } else {
            return new Cookies(resource.headers.cookie);
        }
    }
}
