const genetateANSIColor = require('./genetateANSIColor'),
    normalizeTime = require('./normalizeTime');

module.exports = color => genetateANSIColor(color, `[${(cur => {
    return `${
        normalizeTime(cur.getHours())
    }:${
        normalizeTime(cur.getMinutes())
    }:${
        normalizeTime(cur.getSeconds())
    }`
})(new Date())}] | `)
