import pkg from 'package.json'
import checkType from 'type-checker'
import http from 'http'
import ObjPrivProps from 'node-object-private-props'
import Cookies from '../parts/cookies'
import prefix from '../parts/prefix'
import classGenerator from '../parts/classGenerator'

const { getPrivate, setPrivate } = ObjPrivProps;

const { LeNode, Site: _Site } = classGenerator({
    getPrivate,
    setPrivate,
    prefix,
    http,
    checkType,
    pkg,
    Cookies,
    importer: path => import(path).then(({ default: _ }) => _),
});

export const Site = _Site

export default LeNode
