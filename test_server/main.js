const {LeNode} = require(__dirname + '/..');
new LeNode({
    router: [
        {
            domain: /localhost/,
            folder: '/home/superuser/Документы/GitHub/Lenode-3/test_server/test_site'
        },
        {
            domain: /.*/,
            folder: '/home/superuser/Документы/GitHub/Lenode-3/test_server/extended_site'
        },
    ],
}).start(8080);
