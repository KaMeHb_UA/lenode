const http = require('http'),
    https = require('https'),
    querystring = require('querystring');
function splitURI(uri){
    var [, scheme, host, , port, , location, , parameters, , fragment] = /^(\w+):\/\/([^:/]+)(:(\d+))?((\/[^\?]*?)(\?([^#]*))?(#(.*)?)?)?$/.exec(uri);
    return {scheme, host, port: port * 1, location, parameters: (p=>{var a={};p.split('&').forEach(e=>{e=e.split('=');a[e.shift()]=e.join('=')||null});return a})(parameters), fragment};
}
module.exports = new (class HTTP{
    get(url){
        return new Promise((resolve, reject) => {
            if(url.match(/^https?:\/\//)){
                (url.match(/^https:\/\//) ? https : http).get(url, res => {
                    let data = '';
                    res.setEncoding('utf8');
                    res.on('data', chunk => { data += chunk });
                    res.on('end', () => { resolve(data) })
                }).on('error', reject)
            } else throw new URIError('URI scheme is not http or https')
        })
    }
    post(url, data){
        return new Promise((resolve, reject) => {
            if(url.match(/^https?:\/\//)){
                data = querystring.stringify(data);
                var {scheme, host, port, location, parameters, fragment} = splitURI(url);
                const targetModule = scheme === 'https' ? https : http;
                parameters = (()=>{
                    var res = [];
                    for(var i in parameters) res.push(`${i}=${parameters[i]}`);
                    return res.join('&')
                })()
                var post_req = targetModule.request({
                    host,
                    port: `${port ? port : targetModule === https ? 443 : 80}`,
                    path: `${location}${parameters ? `?${parameters}` : ''}${fragment ? `#${fragment}` : ''}`,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Content-Length': Buffer.byteLength(data)
                    }
                }, res => {
                    let data = '';
                    res.setEncoding('utf8');
                    res.on('data', chunk => { data += chunk });
                    res.on('end', () => { resolve(data) })
                }).on('error', reject);
                post_req.write(post_data);
                post_req.end()
            } else throw new URIError('URI scheme is not http or https')
        })
    }
})()