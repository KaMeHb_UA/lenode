const {Site} = require('lenode'),
    fs = require('fs'),
    errPage = require(`${__dirname}/error_page.js`),
    colors = require('colors/safe'),
    siteName = colors.cyan(__dirname.split('/').pop());

const pageList = (target => {
    var res = {
        list: {},
        default: null
    };
    fs.readdirSync(target).forEach(file => {
        res.list[file] = require(`${target}/${file}`);
        if (!res.default && res.list[file].default === true) res.default = file;
    });
    return res
})(`${__dirname}/pages`);

if(!pageList.default) throw new ReferenceError('Could not determine default page. You need to add to default page file (at the end) next line:\nmodule.exports.default = true\nAborting...');

class myTestSite extends Site{
    init(){
        this.logger.log(`${siteName}: Started successfully. Default page: ${pageList.default}`);
    }
    getPage(args){
        this.logger.log(`${siteName}: requested page ${args.address}`);
        if (args.address == '/') return pageList.list[pageList.default](args); else if (pageList.list[args.address.slice(1)]) return pageList.list[args.address.slice(1)](args); else {
            return (async () => {
                return errPage(404, 'Not Found')
            })()
        }
    }
}
module.exports = myTestSite;
