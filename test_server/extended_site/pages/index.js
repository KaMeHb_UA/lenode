module.exports = async ({address, method, POST, GET, REQUEST, COOKIE, port, protocol, fullAddress, headers, response}) => {
    return {
        result: JSON.stringify({address, method, POST, GET, REQUEST, COOKIE, port, protocol, fullAddress})
    }
};
module.exports.default = true
