const {getPrivate, setPrivate} = require('node-object-private-props'),
    checkType = require('type-checker'),
    pkg = require('package.json'),
    http = require('http'),
    Cookies = require('./parts/cookies'),
    prefix = require('./parts/prefix'),
    classGenerator = require('./parts/classGenerator');

module.exports = classGenerator({
    getPrivate,
    setPrivate,
    prefix,
    http,
    checkType,
    pkg,
    Cookies,
    importer: require,
})
